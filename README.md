# This repo to demonstrate the process to build and deploy a simple Laravel app.
```
.
|-..gitlab-ci.yml
|-.Dockerfile
|-.app
|-laravel-chart
```

# Description
- Update the AWS credentials [AWS_ACCESS_KEY_ID] [AWS_SECRET_ACCESS_KEY] into gitlab CI/CD variables setting to allow deploy app to EKS later on.
- The Dockerfile to copy source code in ./app, build the Laravel application and package into a docker iamge.
- The docker image will be pushed to Gitlab Container Registry in same repo.
- Then the CI will talk to EKS API using AWS creds to trigger Helm deployment.
# Additional requirements
To allow EKS node pull the image from gitlab Regitry, please run following commands:
```
 kubectl create secret docker-registry registry-credentials  \
  --docker-server=https://registry.gitlab.com \
  --docker-username=service-account \
  --docker-email=service-account@gmail.com \
  --docker-password=[Put your gitlab service-account pass here]
  ```
The credentials will be attached in to deployment manifest of Laravel to allow EKS to pull the image.

Create a secret key on AWS secretManager with following content which will be pulled when CI running to create the secret for Laravel pods
```
kind: Secret
apiVersion: v1
metadata:
  name: laravel-app-env
stringData:
  .env: |
    APP_NAME=Laravel
    APP_ENV=production
    APP_KEY=base64:kLHmdtqS0YnTACWSpkV4w1GVOQMEXQ68Usk8WR+yauA=
    APP_DEBUG=true
    APP_URL=http://test.laravel.com

    LOG_CHANNEL=null
    LOG_LEVEL=debug

    DB_CONNECTION=mysql
    DB_HOST=mysql_host
    DB_PORT=3306
    DB_DATABASE=test_db
    DB_USERNAME=admin
    DB_PASSWORD=admin_password

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=file
    SESSION_LIFETIME=120

    MEMCACHED_HOST=127.0.0.1

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_MAILER=smtp
    MAIL_HOST=mailhog
    MAIL_PORT=1025
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS=null
    MAIL_FROM_NAME="${APP_NAME}"

    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1

    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
    ```